import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import People from './pages/People';
import Todos from './pages/Todos';
import Home from './pages/Home';
import Person from './pages/Person';
import Layout from './components/Layout';

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<Home />} />
        <Route path="todos" element={<Todos />} />
        <Route path="people" element={<People />} />
        <Route path="people/:id" element={<Person />} />
        <Route path="*" element={<Navigate to="." />} />
      </Route>
    </Routes>
  );
};

export default App;
