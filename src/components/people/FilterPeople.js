import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { filterPeople } from '../../redux-toolkit/features/peopleSlice';
import { selectPeople } from '../../redux-toolkit/selectors/peopleSelectors';

const FilterPeople = () => {
  const peopleList = useSelector(selectPeople);
  const dispatch = useDispatch();

  const handleClick = (event) => {
    const target = event.target.value.trim();

    if (peopleList && target) {
      dispatch(filterPeople(target));
    }
  };

  return (
    <input
      type="search"
      className="form-control w-25 mx-auto mt-4"
      placeholder="Search"
      onChange={handleClick}
    />
  );
};

export default FilterPeople;
