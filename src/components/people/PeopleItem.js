import React from 'react';
import { useNavigate } from 'react-router-dom';

const PeopleItem = React.memo(
  ({ name, birth_year, idx }) => {
    const navigate = useNavigate();

    const choicePerson = (id) => navigate(`${id}`);

    return (
      <li
        className="list-group-item"
        onClick={() => choicePerson(idx + 1)}
      >
        <p>
          <strong>Name:</strong> {name}
        </p>
        <time className="text-secondary">
          <strong>Birth year:</strong> {birth_year}
        </time>
        <button className="btn btn-sm btn-primary d-block w-25 mx-auto my-2">
          Details
        </button>
      </li>
    );
  },
  (prevProps, nextProps) => {
    if (prevProps.name === nextProps.name) {
      return true;
    } else {
      return false;
    }
  }
);

export default PeopleItem;
