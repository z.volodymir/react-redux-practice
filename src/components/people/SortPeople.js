import React from 'react';
import { useDispatch } from 'react-redux';
import { sortPeople } from '../../redux-toolkit/features/peopleSlice';

const SortPeople = () => {
  const dispatch = useDispatch();

  return (
    <button
      className="btn btn-secondary"
      onClick={() => dispatch(sortPeople())}
    >
      SORT BY NAME
    </button>
  );
};

export default SortPeople;
