import React from 'react';
import { useDispatch } from 'react-redux';
import { fetchPeople } from '../../redux-toolkit/features/peopleSlice';

const FetchPeople = React.memo(() => {
  const dispatch = useDispatch();

  return (
    <button className="btn btn-primary" onClick={() => dispatch(fetchPeople())}>
      FETCH
    </button>
  );
});

export default FetchPeople;
