import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithWrapper } from '../../../utils/utils';
import FilterPeople from '../FilterPeople';

describe('FilterPeople', () => {
  test('should render', () => {
    renderWithWrapper(<FilterPeople />);

    const search = screen.getByRole('searchbox');

    expect(search).toBeInTheDocument();
    expect(search).toHaveAttribute('type', 'search');
  });

  test('should have placeholder', () => {
    renderWithWrapper(<FilterPeople />);

    const search = screen.getByPlaceholderText(/search/i);

    expect(search).toBeInTheDocument();
  });

  test('should type input', async () => {
    const user = userEvent.setup();

    renderWithWrapper(<FilterPeople />);

    const input = screen.getByRole('searchbox');
    await user.type(input, 'Commodo anim culpa');

    expect(input.value).toBe('Commodo anim culpa');
    expect(input).toHaveValue('Commodo anim culpa');
    expect(input).toHaveDisplayValue(/commodo anim culpa/i);
  });
});
