import { screen } from '@testing-library/react';
import { renderWithWrapper } from '../../../utils/utils';
import PeopleItem from '../PeopleItem';

describe('PeopleItem', () => {
  test('should render list item', () => {
    renderWithWrapper(<PeopleItem />);

    const listItem = screen.queryByRole('listitem');

    expect(listItem).toBeInTheDocument();
  });

  test('should render button', () => {
    renderWithWrapper(<PeopleItem />);

    const button = screen.queryByRole('button');

    expect(button).toBeInTheDocument();
    expect(button).toHaveTextContent(/details/i);
  });

  test('should render props', () => {
    renderWithWrapper(
      <PeopleItem idx="1" name="User" birth_year="01.01.1900" />
    );

    const listItem = screen.getByRole('listitem');

    expect(listItem).toHaveTextContent('User');
    expect(listItem).toHaveTextContent('01.01.1900');
  });
});
