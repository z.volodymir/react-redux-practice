import { screen } from "@testing-library/react"
import { renderWithWrapper } from "../../../utils/utils"
import SortPeople from "../SortPeople"

describe('SortPeople', () => {
  test('should render button', () => {
    renderWithWrapper(<SortPeople/>)

    const button = screen.getByRole('button')

    expect(button).toBeInTheDocument()
    expect(button).toHaveTextContent(/sort by name/i)
  })
})