import React from 'react';
import { useSelector } from 'react-redux';
import {
  selectLoadingPeople,
  selectPeople,
} from '../../redux-toolkit/selectors/peopleSelectors';
import Spinner from '../Spinner';
import PeopleItem from './PeopleItem';

const PeopleList = () => {
  const isLoading = useSelector(selectLoadingPeople);
  const peopleList = useSelector(selectPeople);

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        peopleList && (
          <ul className="list-group mx-auto w-50 mt-4">
            {peopleList?.map((person, idx) => {
              return (
                <PeopleItem
                  key={idx}
                  name={person.name}
                  birth_year={person.birth_name}
                  idx={idx}
                />
              );
            })}
          </ul>
        )
      )}
    </>
  );
};

export default PeopleList;
