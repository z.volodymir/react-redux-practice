import { render, screen } from '@testing-library/react';
import Spinner from '../Spinner';

describe('Spinner component', () => {
  test('should render', () => {
    render(<Spinner />);

    const status = screen.getByRole('status');

    expect(status).toBeInTheDocument();
    expect(status).toHaveTextContent(/loading/i);
  });
});
