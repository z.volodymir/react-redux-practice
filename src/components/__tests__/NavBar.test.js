import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithRouter } from '../../utils/utils';
import NavBar from '../NavBar';

describe('NavBar component', () => {
  test('should render navigation', () => {
    renderWithRouter(<NavBar />);

    expect(screen.getByRole('navigation')).toBeInTheDocument();
  });

  test('should render the navbar', () => {
    renderWithRouter(<NavBar />);

    expect(screen.getByRole('list')).toBeInTheDocument();
    expect(screen.getByRole('list')).toHaveClass('navbar-nav');
  });

  test('should render the todos link', () => {
    renderWithRouter(<NavBar />);

    const [todos] = screen.getAllByRole('link');

    expect(todos).toBeInTheDocument();
    expect(todos).toHaveTextContent('Todos');
    expect(todos).toHaveAttribute('href', '/todos');
  });

  test('should render the people link', () => {
    renderWithRouter(<NavBar />);

    const [_, people] = screen.getAllByRole('link');

    expect(people).toBeInTheDocument();
    expect(people).toHaveTextContent('People');
    expect(people).toHaveAttribute('href', '/people');
  });
});

describe('NavLinks', () => {
  const user = userEvent.setup();

  test('should not have active links', () => {
    renderWithRouter(<NavBar />);

    const [todos, people] = screen.getAllByRole('link');

    expect(todos).not.toHaveClass('fw-bold');
    expect(people).not.toHaveClass('fw-bold');
  });

  test('should show active the todos link', async () => {
    renderWithRouter(<NavBar />);

    const [todos, people] = screen.getAllByRole('link');

    await user.click(todos);

    expect(todos).toHaveClass('fw-bold');
    expect(people).not.toHaveClass('fw-bold');
  });

  test('should show active the people link', async () => {
    renderWithRouter(<NavBar />);

    const [todos, people] = screen.getAllByRole('link');

    await user.click(people);

    expect(todos).not.toHaveClass('fw-bold');
    expect(people).toHaveClass('fw-bold');
  });
});
