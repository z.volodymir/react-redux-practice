import { screen } from '@testing-library/react';
import { renderWithRouter } from '../../utils/utils';
import Layout from '../Layout';

describe('Layout component', () => {
  test('should rendering header and main', () => {
    renderWithRouter(<Layout />);

    expect(screen.getByRole('banner')).toBeInTheDocument();
    expect(screen.getByRole('main')).toBeInTheDocument();
  });
});
