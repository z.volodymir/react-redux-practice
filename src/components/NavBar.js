import React from 'react';
import { NavLink } from 'react-router-dom';

const setActive = ({ isActive }) =>
  isActive ? 'nav-link fw-bold' : 'nav-link';

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink className={setActive} to="/todos">
                Todos
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className={setActive} to="/people">
                People
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
