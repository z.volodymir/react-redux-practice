import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithWrapper } from '../../../utils/utils';
import TodoForm from '../TodoForm';

describe('TodoForm', () => {
  const user = userEvent.setup();

  describe('Todo input', () => {
    test('should render input', () => {
      renderWithWrapper(<TodoForm />);

      const input = screen.getByRole('textbox');

      expect(input).toBeInTheDocument();
      expect(screen.getByPlaceholderText(/add todo/i)).toBeInTheDocument();

      expect(input).toBeEmptyDOMElement();
      expect(input).toHaveAttribute('type', 'text');
    });

    test('should focusing input', async () => {
      renderWithWrapper(<TodoForm />);

      const input = screen.getByRole('textbox');

      expect(input).not.toHaveFocus();

      await user.tab(input);

      expect(input).toHaveFocus();
    });

    test('should typing input', async () => {
      renderWithWrapper(<TodoForm />);

      const input = screen.getByRole('textbox');

      expect(input).toBeEmptyDOMElement();
      expect(input).toHaveDisplayValue('');
      expect(input).toHaveValue('');

      await user.type(input, 'Commodo anim culpa');

      expect(input.value).toBe('Commodo anim culpa');
      expect(input).toHaveValue('Commodo anim culpa');
      expect(input).toHaveDisplayValue(/commodo anim culpa/i);
    });
  });

  describe('Todo button', () => {
    test('should render', () => {
      renderWithWrapper(<TodoForm />);

      const button = screen.getByRole('button');

      expect(button).toBeInTheDocument();
      expect(button).toHaveAttribute('type', 'submit');
    });

    test('should clean input after press the button', async () => {
      renderWithWrapper(<TodoForm />);

      const input = screen.getByRole('textbox');
      const button = screen.getByRole('button');

      await user.type(input, 'Commodo anim culpa');

      expect(input).toHaveValue('Commodo anim culpa');
      expect(input).toHaveDisplayValue(/commodo anim culpa/i);

      await user.click(button);

      expect(input).toBeEmptyDOMElement();
      expect(input).toHaveValue('');
      expect(input).toHaveDisplayValue('');
    });
  });
});
