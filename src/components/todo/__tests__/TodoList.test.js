import * as reduxHooks from 'react-redux';
import { renderWithWrapper } from '../../../utils/utils';
import TodoList from '../TodoList';

// jest.mock('react-redux');

const todos = [
  { id: 1, text: 'React', completed: false },
  { id: 2, text: 'Redux', completed: false },
];

const mockedUseSelector = jest.spyOn(reduxHooks, 'useSelector');

describe('Todo list', () => {
  test('should render without todo items', () => {
    mockedUseSelector.mockReturnValue([]);

    expect(renderWithWrapper(<TodoList />)).toMatchSnapshot();
  });

  test('should render with todo items', () => {
    mockedUseSelector.mockReturnValue(todos);

    expect(renderWithWrapper(<TodoList />)).toMatchSnapshot();
  });
});
