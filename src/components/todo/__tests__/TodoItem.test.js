import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/react';
import * as reduxHooks from 'react-redux';
import * as actions from '../../../redux-toolkit/features/todoSlice';
import TodoItem from '../TodoItem';
import { renderWithWrapper } from '../../../utils/utils';

describe('Todo Item', () => {
  test('should render item', () => {
    renderWithWrapper(<TodoItem />);

    const heading = screen.getByRole('heading');

    expect(heading).toBeInTheDocument();
  });

  test('should render button', () => {
    renderWithWrapper(<TodoItem />);

    const [completeButton] = screen.getAllByRole('button');

    expect(completeButton).toBeInTheDocument();
    expect(completeButton).toHaveTextContent(/complete/i);
  });

  test('should render delete button', () => {
    renderWithWrapper(<TodoItem />);

    const [_, deleteButton] = screen.getAllByRole('button');

    expect(deleteButton).toBeInTheDocument();
    expect(deleteButton).toHaveTextContent(/delete/i);
  });
});

const mockedUseDispatch = jest.spyOn(reduxHooks, 'useDispatch');

describe('Todo list with dispatch', () => {
  const user = userEvent.setup();

  test('should render without todo items', () => {
    mockedUseDispatch.mockReturnValue(jest.fn());

    expect(renderWithWrapper(<TodoItem />)).toMatchSnapshot();
  });

  test('should dispatch actions', async () => {
    const dispatch = jest.fn();
    mockedUseDispatch.mockReturnValue(dispatch);

    const mockedSetCompleted = jest.spyOn(actions, 'setCompleted');

    renderWithWrapper(<TodoItem id="1" text="React" completed="false" />);

    const [completeButton, deleteButton] = screen.getAllByRole('button');
    await user.click(completeButton);

    expect(dispatch).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledTimes(1);

    expect(mockedSetCompleted).toHaveBeenCalled();
    expect(mockedSetCompleted).toHaveBeenCalledTimes(1);
    expect(mockedSetCompleted).toHaveBeenCalledWith('1');
  });
});
