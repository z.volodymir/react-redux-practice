import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
// import {addTodo} from "../../redux/actions";
import { addTodo } from '../../redux-toolkit/features/todoSlice';

const TodoForm = () => {
  const [input, setInput] = useState('');

  const dispatch = useDispatch();

  const createTodo = (e) => {
    e.preventDefault();

    if (input.trim()) {
      const newTodo = {
        id: Date.now(),
        text: input,
        completed: false,
      };

      dispatch(addTodo(newTodo));
      setInput('');
    }
  };

  return (
    <form onSubmit={createTodo}>
      <div className="mb-3">
        <input
          type="text"
          className="form-control w-25 mx-auto"
          placeholder="Add TODO"
          value={input}
          onChange={(e) => setInput(e.target.value)}
        />
      </div>
      <button type="submit" className="btn btn-primary">
        Add
      </button>
    </form>
  );
};

export default TodoForm;
