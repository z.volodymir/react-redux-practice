import React from 'react';
import { useDispatch } from 'react-redux';
// import {deleteTodo, setCompleted} from "../../redux/actions";
import {
  deleteTodo,
  setCompleted,
} from '../../redux-toolkit/features/todoSlice';

const TodoItem = React.memo(({ id, text, completed }) => {
  const dispatch = useDispatch();

  return (
    <li className="list-group-item d-flex justify-content-between align-items-center">
      <h3 className={completed ? 'text-decoration-line-through m-0' : 'm-0'}>
        {text}
      </h3>
      <div className="btn-group">
        <button
          className="btn btn-success"
          onClick={() => dispatch(setCompleted(id))}
        >
          Complete
        </button>
        <button
          className="btn btn-danger"
          onClick={() => dispatch(deleteTodo(id))}
        >
          Delete
        </button>
      </div>
    </li>
  );
});

export default TodoItem;
