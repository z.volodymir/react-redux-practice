import React from 'react';
import { useSelector } from 'react-redux';
import { selectTodos } from '../../redux-toolkit/selectors/todoSelectors';
import TodoItem from './TodoItem';

const TodoList = () => {
  const todos = useSelector(selectTodos);

  return (
    <ul className="list-group mt-4 w-50 mx-auto">
      {todos.map(({ text, id, completed }) => (
        <TodoItem key={id} id={id} text={text} completed={completed} />
      ))}
    </ul>
  );
};

export default TodoList;
