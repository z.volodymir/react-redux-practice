import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPeople } from '../redux-toolkit/features/peopleSlice';

const Pagination = React.memo(() => {
  const [currentPage, setCurrentPage] = useState(null);

  const dispatch = useDispatch();
  const data = useSelector((state) => state?.people.people);
  const dataItemsCount = data?.count;

  useEffect(() => {
    if (dataItemsCount && data.next) {
      const activePage = data.next
        ? +data.next.slice(-1) - 1
        : +data.previous.slice(-1) + 1;

      setCurrentPage(activePage);
    }
  }, [data, dataItemsCount]);

  const pagesArray = [];
  if (dataItemsCount) {
    const countPages = Math.floor(dataItemsCount / 10);

    for (let i = 0; i <= countPages; i++) {
      pagesArray.push(i + 1);
    }
  }

  return (
    <>
      {dataItemsCount && (
        <nav className="mt-4">
          <ul className="pagination pagination-sm justify-content-center">
            {pagesArray.map((pageNum) => {
              return (
                <li
                  className={
                    currentPage === pageNum ? 'page-item active' : 'page-item'
                  }
                  role="button"
                  key={pageNum}
                >
                  <button
                    className="page-link"
                    onClick={() => dispatch(fetchPeople(pageNum))}
                  >
                    {pageNum}
                  </button>
                </li>
              );
            })}
          </ul>
        </nav>
      )}
    </>
  );
});

export default Pagination;
