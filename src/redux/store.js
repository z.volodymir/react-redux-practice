import {applyMiddleware, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import {rootReducer} from "./reducers/rootReducer";

//  redux-persist
const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['people', 'person'],
  whitelist: ['todos']
}
const persistedReducer = persistReducer(persistConfig, rootReducer);

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const configureStore = () => createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(thunk)),
)

const store = configureStore();

export const persistor = persistStore(store);

export default store;