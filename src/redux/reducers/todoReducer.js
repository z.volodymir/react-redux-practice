import {
  ADD_TODO,
  DELETE_TODO,
  SET_COMPLETED_TODO,
} from "../types";

const initialState = [
  {id: 1, text: 'React', completed: false},
  {id: 2, text: 'Redux', completed: false}
]

const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return [...state, action.payload]
    case DELETE_TODO:
      return state.filter(todo => todo.id !== action.payload)
    case SET_COMPLETED_TODO:
      return [...state].map(todo => {
        if (todo.id === action.payload) {
          todo.completed = !todo.completed
        }
        return todo
      })
    default:
      return state
  }
}

export default todoReducer