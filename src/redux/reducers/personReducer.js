import {FETCH_PERSON, SET_LOADING_PERSON} from "../types";

const initialState = {
  person: {},
  isLoading: false
}

const personReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PERSON:
      return {
        ...state,
        person: action.payload
      }
    case SET_LOADING_PERSON:
      return {
        ...state,
        isLoading: !state.isLoading
      }
    default:
      return state;
  }
}

export default personReducer