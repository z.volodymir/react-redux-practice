import {FETCH_PEOPLE, SET_LOADING_PEOPLE} from "../types";

const initialState = {
  people: {},
  isLoading: false
}

const peopleReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PEOPLE:
      return {
        ...state,
        people: action.payload
      }
    case SET_LOADING_PEOPLE:
      return {
        ...state,
        isLoading: !state.isLoading
      }
    default:
      return state
  }
}

export default peopleReducer