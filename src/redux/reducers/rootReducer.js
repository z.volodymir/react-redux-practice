import {combineReducers} from "redux";
import peopleReducer from "./peopleReducer";
import todoReducer from "./todoReducer";
import personReducer from "./personReducer";

export const rootReducer = combineReducers({
  people: peopleReducer,
  person: personReducer,
  todos: todoReducer
})