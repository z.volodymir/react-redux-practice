export const FETCH_PEOPLE = 'FETCH_PEOPLE'
export const SET_LOADING_PEOPLE = 'SET_LOADING_PEOPLE'
export const SET_LOADING_PERSON = 'SET_LOADING_PERSON'
export const FETCH_PERSON = 'FETCH_PERSON'
export const ADD_TODO = 'ADD_TODO'
export const DELETE_TODO = 'DELETE_TODO'
export const SET_COMPLETED_TODO = 'SET_COMPLETED_TODO'
