import {
  FETCH_PEOPLE,
  ADD_TODO,
  DELETE_TODO,
  FETCH_PERSON,
  SET_COMPLETED_TODO,
  SET_LOADING_PEOPLE,
  SET_LOADING_PERSON,
} from "./types";

export const addTodo = (todo) => {
  return {type: ADD_TODO, payload: todo}
}

export const deleteTodo = (todo) => {
  return {type: DELETE_TODO, payload: todo}
}

export const setCompleted = (id) => {
  return {type: SET_COMPLETED_TODO, payload: id}
}

export const setLoadingPeople = () => {
  return {type: SET_LOADING_PEOPLE}
}

export const setLoadingPerson = () => {
  return {type: SET_LOADING_PERSON}
}

export const fetchPeople = (page = 1) => {
  return async dispatch => {

    dispatch(setLoadingPeople())

    const request = await fetch(`https://swapi.dev/api/people/?page=${page}`)
    const json = await request.json()

    dispatch({
      type: FETCH_PEOPLE,
      payload: json
    })

    dispatch(setLoadingPeople())
  }
}

export const fetchPerson = (id) => {
  return async dispatch => {

    dispatch(setLoadingPerson())

    const request = await fetch(`https://swapi.dev/api/people/${id}`)
    const json = await request.json()

    dispatch({
      type: FETCH_PERSON,
      payload: json
    })

    dispatch(setLoadingPerson())
  }
}