import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Wrapper from '../components/Wrapper';

export const renderWithRouter = (component) =>
  render(component, { wrapper: BrowserRouter });

export const renderWithWrapper = (component) =>
  render(component, { wrapper: Wrapper });
