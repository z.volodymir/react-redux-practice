import { createReducer, createSlice } from '@reduxjs/toolkit';
// import { addTodo, deleteTodo, setCompleted } from "../actions";

const initialState = [
  { id: 1, text: 'React', completed: false },
  { id: 2, text: 'Redux', completed: false },
];

// const todoReducer = createReducer(initialState, builder => {
//   builder
//     .addCase(addTodo, (state, action) => {
//       state.push(action.payload)
//     })
//     .addCase(deleteTodo, (state, action) => {
//       return state.filter(todo => todo.id !== action.payload)
//     })
//     .addCase(setCompleted, (state, action) => {
//       const todo = state.find(todo => todo.id === action.payload)
//       todo.completed = !todo.completed
//     })
// })

// const todoReducer = createReducer(initialState, {
//   [addTodo]: (state, action) => {
//       state.push(action.payload)
//   },
//   [deleteTodo]: (state, action) => {
//     return state.filter(todo => todo.id !== action.payload)
//   },
//   [setCompleted]: (state, action) => {
//       const todo = state.find(todo => todo.id === action.payload)
//       todo.completed = !todo.completed
//   }
// })

const todoSlice = createSlice({
  name: '@@todos',
  initialState,
  reducers: {
    addTodo: {
      reducer: (state, action) => {
        state.push(action.payload);
      },
      // Not necessarily
      prepare: (todo) => ({
        payload: todo,
      }),
    },
    deleteTodo: {
      reducer: (state, action) => {
        return state.filter((todo) => todo.id !== action.payload);
      },
    },
    setCompleted: {
      reducer: (state, action) => {
        const todo = state.find((todo) => todo.id === action.payload);
        todo.completed = !todo.completed;
      },
    },
  },
});

export const { addTodo, deleteTodo, setCompleted } = todoSlice.actions;

export const todoReducer = todoSlice.reducer;
