import { fetchPeople } from '../peopleSlice';

global.fetch = jest.fn();

describe('peopleThunk', () => {
  test('should fetchPeople with resolved response', async () => {
    const mockPeople = [
      {
        name: 'name',
        birth_year: 'birth_year',
      },
    ];

    fetch.mockResolvedValue({
      ok: true,
      json: () => Promise.resolve(mockPeople),
    });

    const dispatch = jest.fn();
    const thunk = fetchPeople(1);

    await thunk(dispatch, () => ({}));

    console.log(dispatch.mock.calls);

    const { calls } = dispatch.mock;

    expect(calls).toHaveLength(2);

    const [start, end] = calls;

    expect(start[0].type).toBe(fetchPeople.pending().type);
    expect(end[0].type).toBe(fetchPeople.fulfilled().type);
    expect(end[0].payload).toBe(mockPeople);
  });
});
