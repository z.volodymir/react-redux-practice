import { addTodo, deleteTodo, setCompleted, todoReducer } from '../todoSlice';

describe('Todo Slice', () => {
  test('should return default state when we passed an empty action', () => {
    const result = todoReducer([], { type: '' });

    expect(result).toEqual([]);
  });

  test('should add new todo item with "addTodo" action', () => {
    const payload = { id: 1, text: 'React', completed: false };
    const result = todoReducer([], {
      type: addTodo.type,
      payload,
    });

    expect(result[0]).toEqual(payload);
    expect(result[0].id).toBe(1);
    expect(result[0].text).toBe('React');
    expect(result[0].completed).toBe(false);
  });

  test('should toggle todo completed status with "setCompleted" action', () => {
    const state = [{ id: 1, text: 'React', completed: false }];
    const action = { type: setCompleted.type, payload: 1 };
    const result = todoReducer(state, action);

    expect(result[0].completed).toBe(true);
    expect(result[0].completed).toBe(!state[0].completed);
  });

  test('should delete todo by id with "deleteTodo" action', () => {
    const state = [{ id: 1, text: 'React', completed: false }];
    const action = { type: deleteTodo.type, payload: 1 };
    const result = todoReducer(state, action);

    expect(result).toEqual([]);
  });
});
