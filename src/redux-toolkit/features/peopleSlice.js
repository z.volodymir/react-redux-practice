import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const initialState = {
  people: {},
  isLoading: false,
  error: false,
};

export const fetchPeople = createAsyncThunk(
  '@@people/fetchPeople',
  async (page = 1, { dispatch, rejectWithValue }) => {
    // dispatch(setLoadingPeople())

    try {
      const request = await fetch(`https://swapi.dev/api/people/?page=${page}`);
      const data = await request.json();

      return data;
    } catch (error) {
      return rejectWithValue(error.message);
    }

    // dispatch(addPeople(data))
    // dispatch(setLoadingPeople())
  },
  {
    condition: (page, { getState, extra }) => {
      const { isLoading } = getState().people;

      if (isLoading) {
        return false;
      }
    },
  }
);

export const filterPeople = createAsyncThunk(
  '@@people/filterPeople',
  async (value, { dispatch, rejectWithValue }) => {
    try {
      const request = await fetch(
        `https://swapi.dev/api/people/?search=${value}`
      );
      return await request.json();
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
  {
    condition: (_, { getState, extra }) => {
      const { isLoading } = getState().people;

      if (isLoading) {
        return false;
      }
    },
  }
);

const peopleSlice = createSlice({
  name: '@@people',
  initialState,
  reducers: {
    // addPeople: {
    //   reducer: (state, action) => {
    //   },
    //   prepare: (people) => ({payload: people}),
    // },
    // setLoadingPeople: {
    //   reducer: (state) => {
    //     state.isLoading = !state.isLoading
    //   }
    // },
    // filterPeople: {
    //   reducer: (state, action) => {
    //     state.people.results = state.people.results.filter(item => item.name.toLowerCase().includes(action.payload.toLowerCase()))
    //   }
    // }
    sortPeople: {
      reducer: (state, action) => {
        state.people.results.sort((a, b) => {
          return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
        });
      },
    },
  },
  extraReducers: (builder) => {
    builder
      .addMatcher(
        (action) => action.type.endsWith('/pending'),
        (state, action) => {
          state.isLoading = true;
          state.error = false;
        }
      )
      .addMatcher(
        (action) => action.type.endsWith('/rejected'),
        (state, action) => {
          state.isLoading = false;
          state.error = action.payload || action.error.message;
        }
      )
      .addMatcher(
        (action) => action.type.endsWith('/fulfilled'),
        (state, action) => {
          state.people = action.payload;
          state.isLoading = false;
        }
      );
  },
});

// export const {addPeople, setLoadingPeople, filterPeople} = peopleSlice.actions

export const { sortPeople } = peopleSlice.actions;
export const peopleReducer = peopleSlice.reducer;
