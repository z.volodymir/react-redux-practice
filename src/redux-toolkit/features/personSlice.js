import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const initialState = {
  person: {},
  isLoading: false,
  error: false,
};

export const fetchPerson = createAsyncThunk(
  '@@person/fetchPerson',
  async (id, { dispatch, rejectWithValue }) => {
    // dispatch(setLoadingPerson())

    try {
      const request = await fetch(`https://swapi.dev/api/people/${id}`);
      const data = await request.json();

      return data;
    } catch (error) {
      return rejectWithValue(error.message);
    }

    // dispatch(addPerson(data))
    // dispatch(setLoadingPerson())
  },
  {
    condition: (id, { getState, extra }) => {
      const { isLoading } = getState().people;

      if (isLoading) {
        return false;
      }
    },
  }
);

const personSlice = createSlice({
  name: '@@person',
  initialState,
  reducers: {
    //   addPerson: {
    //     reducer: (state, action) => {
    //       state.person = action.payload
    //     },
    //     prepare: (person) => ({payload: person})
    //   },
    //   setLoadingPerson: {
    //     reducer: (state, action) => {
    //       state.isLoading = !state.isLoading
    //     }
    //   }
  },
  // OPTION 1
  extraReducers: (builder) => {
    builder
      .addCase(fetchPerson.pending, (state, action) => {
        state.isLoading = true;
        state.error = false;
      })
      .addCase(fetchPerson.fulfilled, (state, action) => {
        state.person = action.payload;
        state.isLoading = false;
      })
      .addMatcher(
        (action) => action.type.endsWith('/rejected'),
        (state, action) => {
          state.isLoading = false;
          state.error = action.payload || action.error.message;
        }
      );
  },
  // OPTION 2
  // extraReducers: {
  //   [fetchPerson.fulfilled]: (state, action) => {
  //     state.person = action.payload;
  //     state.isLoading = false;
  //   }
  // }
});

// export const {addPerson, setLoadingPerson} = personSlice.actions

export const personReducer = personSlice.reducer;
