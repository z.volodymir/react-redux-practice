import { configureStore } from '@reduxjs/toolkit';
import {
    persistStore,
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { rootReducer } from './reducers';

//  redux-persist
const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['people', 'person'],
    whitelist: ['todos'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
    reducer: persistedReducer,
    devTools: true,
    enhancers: [],
    // preloadedState: [],
    // middleware: (getDefaultMiddlware) => getDeafaultMiddlware().concat(SOME_MIDDLEWARE),
    middleware: (getDefaultMiddlware) =>
        getDefaultMiddlware({
            serializableCheck: {
                ignoredActions: [
                    FLUSH,
                    REHYDRATE,
                    PAUSE,
                    PERSIST,
                    PURGE,
                    REGISTER,
                ],
            },
        }),
});

export const persistor = persistStore(store);

export default store;
