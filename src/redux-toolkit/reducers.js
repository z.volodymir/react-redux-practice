import {combineReducers} from "redux";
import {todoReducer} from "./features/todoSlice";
import {peopleReducer} from "./features/peopleSlice";
import {personReducer} from "./features/personSlice";

export const rootReducer = combineReducers({
  people: peopleReducer,
  person: personReducer,
  todos: todoReducer
})