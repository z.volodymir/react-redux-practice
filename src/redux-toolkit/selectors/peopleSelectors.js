export const selectPeople = (state) => state.people.people.results;
export const selectLoadingPeople = (state) => state?.people.isLoading;
