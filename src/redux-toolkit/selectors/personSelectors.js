export const selectPerson = (state) => state.person.person;
export const selectLoadingPerson = (state) => state.person.isLoading;
