import { selectTodos } from '../todoSelectors';

describe('Todo selectors', () => {
  test('should select todo from the state', () => {
    const todos = [{ id: 1, text: 'React', completed: false }];
    const result = selectTodos({ todos: todos });

    expect(result).toEqual(todos);
  });
});
