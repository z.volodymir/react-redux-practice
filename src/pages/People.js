import React from 'react';
import Pagination from '../components/Pagination';
import FilterPeople from '../components/people/FilterPeople';
import SortPeople from '../components/people/SortPeople';
import FetchPeople from '../components/people/FetchPeople';
import PeopleList from '../components/people/PeopleList';

const People = () => {
  return (
    <div className="text-center mt-4">
      <h1>SWAPI (people)</h1>
      <div className="btn-group">
        <FetchPeople />
        <SortPeople />
      </div>
      <FilterPeople />
      <PeopleList />
      <Pagination />
    </div>
  );
};

export default People;
