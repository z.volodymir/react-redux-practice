import React, { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPerson } from '../redux-toolkit/features/personSlice';
import Spinner from '../components/Spinner';
import {
  selectLoadingPerson,
  selectPerson,
} from '../redux-toolkit/selectors/personSelectors';

const Person = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const person = useSelector(selectPerson);
  const isLoading = useSelector(selectLoadingPerson);

  useEffect(() => {
    dispatch(fetchPerson(id));
  }, [id, dispatch]);

  return (
    <div className="d-flex flex-column align-items-center gap-4 mt-4">
      {isLoading ? (
        <Spinner />
      ) : (
        person && (
          <div className="card w-25">
            <h5 className="card-header w-100">{person.name}</h5>
            <div className="card-body">
              <p className="card-text">
                <strong>Gender:</strong> {person.gender}
              </p>
              <p className="card-text">
                <strong>Eye color:</strong> {person.eye_color}
              </p>
              <p className="card-text">
                <strong>Height:</strong> {person.height}
              </p>
              <p className="card-text">
                <strong>Mass:</strong> {person.mass}
              </p>
            </div>
            <button className="btn btn-secondary" onClick={() => navigate(-1)}>
              Go back
            </button>
          </div>
        )
      )}
    </div>
  );
};

export default Person;
