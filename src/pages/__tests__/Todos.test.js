import { screen } from '@testing-library/react';
import { renderWithWrapper } from '../../utils/utils';
import Todos from '../Todos';

describe('Home page component', () => {
  test('should render heading', () => {
    renderWithWrapper(<Todos />);

    const [title] = screen.getAllByRole('heading');

    expect(title).toBeInTheDocument();
    expect(title).toHaveTextContent(/todo list/i);
  });
});
