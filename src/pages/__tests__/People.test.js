import { screen } from '@testing-library/react';
import { renderWithWrapper } from '../../utils/utils';
import People from '../People';

describe('People page', () => {
  test('should render', () => {
    renderWithWrapper(<People />);
  });

  test('should render title', () => {
    renderWithWrapper(<People />);

    const title = screen.getByRole('heading');

    expect(title).toBeInTheDocument();
    expect(title).toHaveTextContent('SWAPI (people)');
  });
});
