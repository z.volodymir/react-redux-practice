import { render, screen } from '@testing-library/react';
import Home from '../Home';

describe('Home page component', () => {
  test('should render', () => {
    render(<Home />);

    expect(screen.getByRole('heading')).toBeInTheDocument();
    expect(screen.getByRole('heading')).toHaveTextContent(/home page/i);
  });
});
