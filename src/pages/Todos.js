import React from 'react';
import TodoList from "../components/todo/TodoList";
import TodoForm from "../components/todo/TodoForm";

const Todos = () => {

  return (
    <div className="text-center mt-4">
      <h1>TODO list</h1>
      <TodoForm/>
      <TodoList/>
    </div>
  );
};

export default Todos;