import { createRoot } from 'react-dom/client';
import Wrapper from './components/Wrapper';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';

const container = document.getElementById('root');
const root = createRoot(container);
root.render(
  <Wrapper>
    <App />
  </Wrapper>
);
