import { render, screen } from '@testing-library/react';
import { MemoryRouter, useLocation } from 'react-router-dom';
import { renderWithWrapper } from './utils/utils';
import App from './App';

describe('App component', () => {
  const LocationDisplay = () => {
    const location = useLocation();

    return <div data-testid="location-display">{location.pathname}</div>;
  };

  test('should render App component', () => {
    renderWithWrapper(<App />);
  });

  test('should render component that uses useLocation', () => {
    const route = '/';

    // use <MemoryRouter> when you want to manually control the history
    render(
      <MemoryRouter initialEntries={[route]}>
        <LocationDisplay />
      </MemoryRouter>
    );

    // verify location display is rendered
    expect(screen.getByTestId('location-display')).toHaveTextContent(route);
  });

  test('should not render a component that uses useLocation', () => {
    const route = '/';

    // use <MemoryRouter> when you want to manually control the history
    render(
      <MemoryRouter initialEntries={[route]}>
        <LocationDisplay />
      </MemoryRouter>
    );

    // verify location display is rendered
    expect(screen.queryByTestId('location-display')).not.toHaveTextContent(
      '/path'
    );
  });
});
